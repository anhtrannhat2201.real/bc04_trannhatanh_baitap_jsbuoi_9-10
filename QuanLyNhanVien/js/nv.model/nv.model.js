const SEP = "sep";
const TRUONGPHONG = "truongphong";
const NV = "nhanvien";
function NhanVien(
  _taikhoan,
  _ten,
  _email,
  _matkhau,
  _ngaythang,
  _luong,
  _chucvu,
  _giolam
) {
  this.taiKhoan = _taikhoan;
  this.ten = _ten;
  this.email = _email;
  this.matkhau = _matkhau;
  this.ngaythang = _ngaythang;
  this.luong = _luong;
  this.chucvu = _chucvu;
  this.giolam = _giolam;
  this.tinhTongLuong = function () {
    if (this.chucvu == SEP) {
      return this.luong * 3;
    }
    if (this.chucvu == TRUONGPHONG) {
      return this.luong * 2;
    }
    if (this.chucvu == NV) {
      return this.luong * 1;
    }
  };
  this.xepLoaiNV = function () {
    if (this.giolam >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.giolam >= 176) {
      return "Nhân viên giỏi";
    } else if (this.giolam >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
}
