var dsnv = [];
var dsnvJSON = localStorage.getItem("DSNV");
// console.log("dsnvJSON: ", dsnvJSON);
if (dsnvJSON != null) {
  dsnv = JSON.parse(dsnvJSON);
  // console.log("dsnv: ", dsnv);
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    // console.log("nv: ", nv);
    dsnv[index] = new NhanVien(
      nv.taiKhoan,
      nv.ten,
      nv.email,
      nv.matkhau,
      nv.ngaythang,
      nv.luong,
      nv.chucvu,
      nv.giolam
    );
    // console.log("NHANVIEN: ", dsnv[index]);
  }
  renderDSNV(dsnv);
}
// Nút thêm nhân viên
document.getElementById("btnThemNV").addEventListener("click", function () {
  // console.log("yes");
  var newNV = layThongTinTuForm();

  //kiểm tra validation tài khoản
  let isValid =
    validator.kiemTraRong(
      newNV.taiKhoan,
      "tbTKNV",
      "Tài khoản không được để trống"
    ) &&
    validator.checkDuplicate(
      newNV.taiKhoan,
      dsnv,
      "tbTKNV",
      "Tài khoản đã được sử dụng, vui lòng chọn tài khoản khác"
    ) &&
    validator.kiemTraTaiKhoan(
      newNV.taiKhoan,
      "tbTKNV",
      "Tài khoản chỉ được tối đa 4 đến 6 kí tự",
      3,
      7
    );
  //kiểm tra validation họ tên
  isValid &=
    validator.kiemTraRong(
      newNV.ten,
      "tbTen",
      "Tên nhân viên không được để trống"
    ) &&
    validator.kiemtraTenNV(
      newNV.ten,
      "tbTen",
      "Không đúng định dạng vui lòng nhập lại"
    );
  // kiểm tra validation email
  isValid &=
    validator.kiemTraRong(newNV.email, "tbEmail", "Chưa nhập Email") &&
    validator.kiemtraEmail(
      newNV.email,
      "tbEmail",
      "Email không đúng định dạng"
    );
  // kiểm tra mật khẩu
  isValid &=
    validator.kiemTraRong(newNV.matkhau, "tbMatKhau", "Chưa nhập password") &&
    validator.kiemtraPass(
      newNV.matkhau,
      "tbMatKhau",
      "Chưa nhập đúng định dạng"
    );
  isValid &= validator.kiemTraRong(
    newNV.ngaythang,
    "tbNgay",
    "Ngày tháng không được để trống"
  );
  isValid &=
    validator.kiemTraRong(newNV.luong, "tbLuongCB", "Chưa nhập lương") &&
    validator.kiemTraLuong(
      newNV.luong,
      "tbLuongCB",
      "Lương phải từ 1,000,000VND-20,000,000VND",
      1000000,
      20000000
    );
  isValid &= validator.kiemtraChucVu(
    newNV.chucvu,
    "tbChucVu",
    "Chưa chọn chức vụ hợp lệ! Vui lòng nhập lại"
  );
  isValid &=
    validator.kiemTraRong(newNV.giolam, "tbGiolam", "Chưa nhập giờ làm") &&
    validator.kiemtraGioLam(
      newNV.giolam,
      "tbGiolam",
      "Nhập sai!Số giờ làm trong tháng phải từ 80-200 giờ",
      80,
      200
    );
  // console.log("newNV: ", newNV);
  if (isValid) {
    // console.log("dsnv: ", dsnv);
    dsnv.push(newNV);
    var dsnvJSON = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dsnvJSON);
    renderDSNV(dsnv);
    xoaThongTinTrenForm();
  }
});
// Nút cập nhật nhân viên
document.getElementById("btnCapNhat").addEventListener("click", function () {
  // console.log("yes");
  let updateNV = layThongTinTuForm();
  // console.log("updateNV: ", updateNV);
  let isValid =
    validator.kiemTraRong(
      updateNV.ten,
      "tbTen",
      "Tên nhân viên không được để trống"
    ) &&
    validator.kiemtraTenNV(
      updateNV.ten,
      "tbTen",
      "Không đúng định dạng vui lòng nhập lại"
    );
  // kiểm tra validation email
  isValid &=
    validator.kiemTraRong(updateNV.email, "tbEmail", "Chưa nhập Email") &&
    validator.kiemtraEmail(
      updateNV.email,
      "tbEmail",
      "Email không đúng định dạng"
    );
  // kiểm tra mật khẩu
  isValid &=
    validator.kiemTraRong(
      updateNV.matkhau,
      "tbMatKhau",
      "Chưa nhập password"
    ) &&
    validator.kiemtraPass(
      updateNV.matkhau,
      "tbMatKhau",
      "Chưa nhập đúng định dạng"
    );
  isValid &= validator.kiemTraRong(
    updateNV.ngaythang,
    "tbNgay",
    "Ngày tháng không được để trống"
  );
  isValid &=
    validator.kiemTraRong(updateNV.luong, "tbLuongCB", "Chưa nhập lương") &&
    validator.kiemTraLuong(
      updateNV.luong,
      "tbLuongCB",
      "Lương phải từ 1,000,000VND-20,000,000VND",
      1000000,
      20000000
    );
  isValid &= validator.kiemtraChucVu(
    updateNV.chucvu,
    "tbChucVu",
    "Chưa chọn chức vụ hợp lệ! Vui lòng nhập lại"
  );
  isValid &=
    validator.kiemTraRong(updateNV.giolam, "tbGiolam", "Chưa nhập giờ làm") &&
    validator.kiemtraGioLam(
      updateNV.giolam,
      "tbGiolam",
      "Nhập sai!Số giờ làm trong tháng phải từ 80-200 giờ",
      80,
      200
    );
  let ntaikhoan = updateNV.taiKhoan;
  // console.log("taiKhoan: ", taikhoan);
  let index = timKiemViTri(ntaikhoan, dsnv);
  if (index != -1) {
    if (isValid) {
      // vị trí index lấy được của dòng nhân viên hiện tại trong dsnv

      // console.log("currentNV: ", currentNV);
      // gắn vị trí hiện tại cho updateNV lấy được từ form
      dsnv[index] = updateNV;
      // băm dữ liệu từ Array thành JSON
      let dsnvJSON = JSON.stringify(dsnv);
      // Lưu json dsnv vào localstorage
      localStorage.setItem("DSNV", dsnvJSON);
      renderDSNV(dsnv);
      xoaThongTinTrenForm();
    }
  }
});
// hàm xóa nhân viên
function xoaNhanVien(id) {
  // console.log("yes");
  var index = timKiemViTri(id, dsnv);
  if (index != -1) {
    // splice dùng để xóa 1 index trong dsnv
    dsnv.splice(index, 1);
    let dsnvJSON = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dsnvJSON);
    renderDSNV(dsnv);
  }
}
// hàm show thông tin
function showNhanVien(id) {
  var index = timKiemViTri(id, dsnv);
  if (index != -1) {
    var currentNV = dsnv[index];
    showThongTinLenForm(currentNV);
  }
}
// tìm nhân viên theo xếp loại
var search = document.getElementById("searchName");
search.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    event.preventDefault;
    document.getElementById("btnTimNV").click();
  }
});
//onclick tìm
document.getElementById("btnTimNV").onclick = function () {
  // console.log("yes");
  var loaiXepHang = document.getElementById("searchName").value;
  if (loaiXepHang == "") {
    renderDSNV(dsnv);
  } else {
    var filterDSNV = dsnv.filter(function (nv) {
      return nv.xepLoaiNV() == loaiXepHang;
    });
    renderDSNV(filterDSNV);
  }
};
// nút đóng
document.getElementById("btnDong").addEventListener("click", function () {
  xoaThongTinTrenForm();
});
