// hàm lấy thông tin từ form
function layThongTinTuForm() {
  const taiKhoan = document.getElementById("tknv").value;
  const hoTen = document.getElementById("name").value;
  const email = document.getElementById("email").value;
  const matKhau = document.getElementById("password").value;
  const ngayThang = document.getElementById("datepicker").value;
  const luongCB = document.getElementById("luongCB").value;
  const chucVu = document.getElementById("chucvu").value;
  const gioLam = document.getElementById("gioLam").value;

  let nv = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayThang,
    luongCB,
    chucVu,
    gioLam
  );
  return nv;
}
// render danh sách nhân viên
function renderDSNV(nvArr) {
  // tạo chuỗi content html rỗng
  let contentHTML = "";
  nvArr.forEach((nv) => {
    let contentTr = `
    <tr>
        <td>${nv.taiKhoan}</td>
        <td>${nv.ten}</td>
        <td>${nv.email}</td>
        <td>${nv.ngaythang}</td>
        <td>${nv.chucvu}</td>
        <td>${nv.tinhTongLuong()}</td>
        <td>${nv.xepLoaiNV()}</td>
        <td>    
        <a class="text-warning" style="cursor:pointer" data-target="#myModal" data-toggle="modal">
        <i class="fa-regular fa-pen-to-square" onclick=showNhanVien('${
          nv.taiKhoan
        }')></i>
        </a>
        <a class="text-danger" style="cursor:pointer">
        <i class="fa-regular fa-trash-can" onclick=xoaNhanVien('${
          nv.taiKhoan
        }')></i>
        </a>
        </td>
    </tr>
    `;
    contentHTML += contentTr;
  });
  // console.log("contentHTML: ", contentHTML);
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
// hàm tìm kiếm vị trí
function timKiemViTri(id, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    let nv = dsnv[index];
    if (nv.taiKhoan == id) {
      return index;
    }
  }
  // nếu không tìm được vị trí thì trả lại -1
  return -1;
}
// hàm show thông tin lên form

function showThongTinLenForm(nv) {
  // console.log("yes");
  (document.getElementById("tknv").value = nv.taiKhoan),
    (document.getElementById("tknv").disabled = true),
    (document.getElementById("name").value = nv.ten),
    (document.getElementById("email").value = nv.email),
    (document.getElementById("password").value = nv.matkhau),
    (document.getElementById("datepicker").value = nv.ngaythang),
    (document.getElementById("luongCB").value = nv.luong),
    (document.getElementById("chucvu").value = nv.chucvu),
    (document.getElementById("gioLam").value = nv.giolam);
}
//hàm check tài khoản không trùng
function checkTaiKhoan(nv, dsnv) {
  var isValid = validatior.checkDuplicate(
    nv.username,
    dsnv,
    "tbTKNV",
    "Tài khoản đã được sử dụng, vui lòng chọn tài khoản khác"
  );
  return isValid;
}
// xóa thông tin trên form
function xoaThongTinTrenForm() {
  (document.getElementById("tknv").value = ""),
    (document.getElementById("tknv").disabled = false);
  (document.getElementById("name").value = ""),
    (document.getElementById("email").value = ""),
    (document.getElementById("password").value = ""),
    (document.getElementById("datepicker").value = ""),
    (document.getElementById("luongCB").value = ""),
    (document.getElementById("chucvu").value = ""),
    (document.getElementById("gioLam").value = "");
}
